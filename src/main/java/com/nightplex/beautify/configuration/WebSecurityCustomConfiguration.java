package com.nightplex.beautify.configuration;

import com.nightplex.beautify.security.RestAuthenticationEntryPoint;
import com.nightplex.beautify.security.filter.JWTAuthenticationFilter;
import com.nightplex.beautify.security.filter.JWTAuthorizationFilter;
import com.nightplex.beautify.security.google2fa.CustomAuthenticationProvider;
import com.nightplex.beautify.services.ResponseHandlingService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Resource;

import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

/**
 * Created by NightPlex on 4/14/2019
 * Github: https://github.com/NightPlex
 */
@Configuration
@EnableWebSecurity
public class WebSecurityCustomConfiguration extends WebSecurityConfigurerAdapter {

    @Resource
    private UserDetailsService userDetailsService;

    @Resource
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    @Resource
    private ResponseHandlingService responseHandlingService;

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider());
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .sessionManagement()
                .sessionCreationPolicy(STATELESS)
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(restAuthenticationEntryPoint)
                .and()
                .authorizeRequests()
                .antMatchers("/test2").authenticated()
                .antMatchers("/test1").hasAuthority("ADMIN_PRIVILEGE")
                .and()
                .addFilter(new JWTAuthenticationFilter(authenticationManager(), responseHandlingService))
                .addFilter(new JWTAuthorizationFilter(authenticationManager(), userDetailsService, responseHandlingService));
        // @formatter:on
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        final CustomAuthenticationProvider authProvider = new CustomAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(encoder());
        return authProvider;
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(11);
    }

}
