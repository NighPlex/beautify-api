package com.nightplex.beautify.configuration;

import com.nightplex.beautify.persistence.entity.Privilege;
import com.nightplex.beautify.persistence.entity.Role;
import com.nightplex.beautify.persistence.entity.User;
import com.nightplex.beautify.persistence.repository.PrivilegeRepository;
import com.nightplex.beautify.persistence.repository.RoleRepository;
import com.nightplex.beautify.persistence.repository.UserRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * Created by NightPlex on 4/14/2019
 * Github: https://github.com/NightPlex
 */
@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private boolean alreadySetup = false;

    @Resource
    private UserRepository userRepository;

    @Resource
    private RoleRepository roleRepository;

    @Resource
    private PrivilegeRepository privilegeRepository;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        if (alreadySetup) {
            return;
        }

        // == create initial privileges
        final Privilege adminPrivilege = createPrivilegeIfNotFound("ADMIN_PRIVILEGE");
        final Privilege clientPrivilege = createPrivilegeIfNotFound("CLIENT_PRIVILEGE");
        final Privilege userPrivilege = createPrivilegeIfNotFound("USER_PRIVILEGE");

        // == create initial roles
        final List<Privilege> adminPrivileges = new ArrayList<>(Arrays.asList(adminPrivilege, clientPrivilege, userPrivilege));
        final Role clientRole = createRoleIfNotFound("ROLE_CLIENT", Collections.singleton(clientPrivilege));
        final Role adminRole = createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
        final Role userRole = createRoleIfNotFound("ROLE_USER", Collections.singleton(userPrivilege));

        // == create initial user
       createUserIfNotFound("admin", "Test", "Test", "test", Collections.singleton(adminRole));
       createUserIfNotFound("client", "Test", "Test", "test", Collections.singleton(clientRole));
       createUserIfNotFound("user", "Test", "Test", "test", Collections.singleton(userRole));

        alreadySetup = true;
    }

    @Transactional
    public Privilege createPrivilegeIfNotFound(final String name) {
        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege(name);
            privilege = privilegeRepository.save(privilege);
        }
        return privilege;
    }

    @Transactional
    public Role createRoleIfNotFound(final String name, final Collection<Privilege> privileges) {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name);
        }
        role.setPrivileges(privileges);
        role = roleRepository.save(role);
        return role;
    }

    @Transactional
    public User createUserIfNotFound(final String email, final String firstName, final String lastName, final String password, final Collection<Role> roles) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            user = new User();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setPassword(passwordEncoder.encode(password));
            user.setEmail(email);
            user.setEnabled(true);
        }
        user.setRoles(roles);
        user = userRepository.save(user);
        return user;
    }

}