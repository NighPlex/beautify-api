package com.nightplex.beautify.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by NightPlex on 4/14/2019
 * Github: https://github.com/NightPlex
 */
@Configuration
@EnableScheduling
public class SpringTaskConfig {

}
