package com.nightplex.beautify.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by NightPlex on 1/25/2019
 * Github: https://github.com/NightPlex
 */
@Configuration
@EnableTransactionManagement
@PropertySource({ "classpath:persistence.properties" })
public class PersistenceJPAConfig {

    public PersistenceJPAConfig() {
        super();
    }

}
