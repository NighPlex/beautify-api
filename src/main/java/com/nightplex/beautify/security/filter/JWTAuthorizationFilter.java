package com.nightplex.beautify.security.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.nightplex.beautify.constants.enums.ErrorResponse;
import com.nightplex.beautify.security.CustomUserDetailsService;
import com.nightplex.beautify.services.ResponseHandlingService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.nightplex.beautify.constants.SecurityConstants.HEADER_STRING;
import static com.nightplex.beautify.constants.SecurityConstants.CLIENT_SECRET;
import static com.nightplex.beautify.constants.SecurityConstants.TOKEN_PREFIX;


/**
 * Created by NightPlex on 4/19/2019
 * Github: https://github.com/NightPlex
 */
public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private CustomUserDetailsService userDetailsService;

    private ResponseHandlingService responseHandlingService;

    public JWTAuthorizationFilter(AuthenticationManager authManager, UserDetailsService userDetailsService, ResponseHandlingService responseHandlingService) {
        super(authManager);
        this.responseHandlingService = responseHandlingService;
        this.userDetailsService = (CustomUserDetailsService) userDetailsService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(HEADER_STRING);

        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req, res);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }


    /**
     *
     * @param request
     * @return User credential based on token
     */
    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request, HttpServletResponse res) {
        String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            // parse the token.
            try {
                String user = JWT.require(Algorithm.HMAC512(CLIENT_SECRET.getBytes()))
                        .build()
                        .verify(token.replace(TOKEN_PREFIX, ""))
                        .getSubject();

                if (user != null) {
                    return userDetailsService.createUserFromToken(user);
                }
                return null;
            } catch (TokenExpiredException e) {
                responseHandlingService.writeErrorMessage(res, e.getMessage(), ErrorResponse.TOKEN_EXPIRED);
                return null;
            } catch (Exception e) {
                responseHandlingService.writeErrorMessage(res, e.getMessage(), ErrorResponse.GENERIC_ERROR);
            }
        }
        responseHandlingService.writeErrorMessage(res, "Missing token from the header", ErrorResponse.MISSING_TOKEN);
        return null;
    }
}