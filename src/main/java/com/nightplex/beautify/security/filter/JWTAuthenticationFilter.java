package com.nightplex.beautify.security.filter;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nightplex.beautify.constants.enums.ErrorResponse;
import com.nightplex.beautify.persistence.entity.User;
import com.nightplex.beautify.services.ResponseHandlingService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static com.nightplex.beautify.constants.SecurityConstants.*;


/**
 * Created by NightPlex on 4/19/2019
 * Github: https://github.com/NightPlex
 */
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authenticationManager;
    private ResponseHandlingService responseHandlingService;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, ResponseHandlingService responseHandlingService) {
        this.authenticationManager = authenticationManager;
        this.responseHandlingService = responseHandlingService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) {
        try {
            User creds = new ObjectMapper()
                    .readValue(req.getInputStream(), User.class);

            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            creds.getEmail(),
                            creds.getPassword(),
                            new ArrayList<>())
            );
        } catch (BadCredentialsException e) {
            responseHandlingService.writeErrorMessage(res, e.getMessage(), ErrorResponse.INVALID_CREDENTIALS);
        } catch (Exception e) {
            responseHandlingService.writeErrorMessage(res, e.getMessage(), ErrorResponse.GENERIC_ERROR);
        }
        return null;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) {
        String token = JWT.create()
                .withSubject(((User) auth.getPrincipal()).getEmail())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(HMAC512(CLIENT_SECRET.getBytes()));
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
        responseHandlingService.writeSuccessLoginMessage(res, "Successfully signed in and authorized", auth.getAuthorities(), TOKEN_PREFIX + token);
    }
}
