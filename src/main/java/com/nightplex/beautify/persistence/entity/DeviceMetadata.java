package com.nightplex.beautify.persistence.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
/**
 * Created by NightPlex on 2/9/2019
 * Github: https://github.com/NightPlex
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class DeviceMetadata {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long userId;
    private String deviceDetails;
    private String location;
    private Date lastLoggedIn;

}