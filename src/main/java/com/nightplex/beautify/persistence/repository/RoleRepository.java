package com.nightplex.beautify.persistence.repository;

import com.nightplex.beautify.persistence.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by NightPlex on 2/9/2019
 * Github: https://github.com/NightPlex
 */
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);

    @Override
    void delete(Role role);
}
