package com.nightplex.beautify.persistence.repository;

import com.nightplex.beautify.persistence.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by NightPlex on 2/9/2019
 * Github: https://github.com/NightPlex
 */
public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);

    @Override
    void delete(User user);
}
