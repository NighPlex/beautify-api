package com.nightplex.beautify.persistence.repository;

import com.nightplex.beautify.persistence.entity.DeviceMetadata;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by NightPlex on 2/10/2019
 * Github: https://github.com/NightPlex
 */
public interface DeviceMetadataRepository extends JpaRepository<DeviceMetadata, Long> {

    List<DeviceMetadata> findByUserId(Long userId);

}
