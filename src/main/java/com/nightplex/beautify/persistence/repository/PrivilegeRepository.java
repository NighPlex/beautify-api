package com.nightplex.beautify.persistence.repository;

import com.nightplex.beautify.persistence.entity.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by NightPlex on 2/9/2019
 * Github: https://github.com/NightPlex
 */
public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {

    Privilege findByName(String name);

    @Override
    void delete(Privilege privilege);
}
