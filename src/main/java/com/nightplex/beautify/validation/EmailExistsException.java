package com.nightplex.beautify.validation;

/**
 * Created by NightPlex on 3/14/2019
 * Github: https://github.com/NightPlex
 */
public class EmailExistsException extends Throwable {

    public EmailExistsException(final String message) {
        super(message);
    }

}
