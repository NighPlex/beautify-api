package com.nightplex.beautify.services;

import com.fasterxml.jackson.databind.util.ArrayBuilders;
import com.nightplex.beautify.constants.enums.ErrorResponse;
import com.nightplex.beautify.constants.enums.SuccessResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

/**
 * Created by NightPlex on 4/21/2019
 * Github: https://github.com/NightPlex
 */
@Service
public class ResponseHandlingService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private JsonObjectBuilder generateMessage(String message, String code, String type) {
        return Json.createObjectBuilder()
                .add("type", type)
                .add("code", code)
                .add("message", message);
    }

    private JsonArrayBuilder createPrivileges(Collection<? extends GrantedAuthority> grantedAuthorities) {
        JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
        grantedAuthorities.forEach(a -> jsonArrayBuilder.add(a.getAuthority()));
        return jsonArrayBuilder;
    }

    private String generateLoginSuccessMessage(String message, Collection<? extends GrantedAuthority> grantedAuthorities, String token) {
        return generateMessage(message, SuccessResponse.LOGIN_SUCCESSFUL.name(), SuccessResponse.SUCCESS.name())
                .add("privileges", createPrivileges(grantedAuthorities))
                .add("token", token).build().toString();
    }

    public void writeErrorMessage(HttpServletResponse res, String message, ErrorResponse errorResponse) {
        writeMessage(res, generateMessage(message, errorResponse.name(), ErrorResponse.ERROR.name()).build().toString());
    }

    private void writeMessage(HttpServletResponse res, String output) {
        try {
            res.setContentType("application/json");
            res.setCharacterEncoding("UTF-8");
            res.setStatus(HttpStatus.OK.value());
            res.getOutputStream().write(output.getBytes());
            res.getOutputStream().close();
        } catch (IOException e) {
            logger.error("Io exception writing the json to response", e);
        }
    }

    public void writeSuccessLoginMessage(HttpServletResponse res, String message, Collection<? extends GrantedAuthority> grantedAuthorities, String token) {
        writeMessage(res, generateLoginSuccessMessage(message,grantedAuthorities, token));
    }

}
