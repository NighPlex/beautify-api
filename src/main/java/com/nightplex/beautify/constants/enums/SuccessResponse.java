package com.nightplex.beautify.constants.enums;

/**
 * Created by NightPlex on 4/21/2019
 * Github: https://github.com/NightPlex
 */
public enum SuccessResponse {

    SUCCESS,
    LOGIN_SUCCESSFUL;

    SuccessResponse() {
    }
}
