package com.nightplex.beautify.constants.enums;

/**
 * Created by NightPlex on 4/21/2019
 * Github: https://github.com/NightPlex
 */
public enum ErrorResponse {

    ERROR,
    TOKEN_EXPIRED,
    INVALID_TOKEN,
    INVALID_CREDENTIALS,
    MISSING_TOKEN,
    GENERIC_ERROR;

    ErrorResponse() {
    }
}
