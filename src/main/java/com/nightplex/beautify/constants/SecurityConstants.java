package com.nightplex.beautify.constants;

/**
 * Created by NightPlex on 4/19/2019
 * Github: https://github.com/NightPlex
 */
public class SecurityConstants  {

    public static final String CLIENT_SECRET = "xzybvdzspcskjidhoxeh";
    public static final long EXPIRATION_TIME = 100; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/rest/v1/register";

}
