package com.nightplex.beautify.api.dto;

import com.nightplex.beautify.validation.ValidPassword;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by NightPlex on 1/20/2019
 * Github: https://github.com/NightPlex
 */
@Getter
@Setter
public class PasswordDto {

    private String oldPassword;

    @ValidPassword
    private String newPassword;

}
