package com.nightplex.beautify.api.controller.v1.base;

import com.auth0.jwt.JWT;
import com.nightplex.beautify.constants.enums.ErrorResponse;
import com.nightplex.beautify.persistence.entity.User;
import com.nightplex.beautify.services.ResponseHandlingService;
import com.nightplex.beautify.services.UserService;
import org.omg.PortableInterceptor.USER_EXCEPTION;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static com.nightplex.beautify.constants.SecurityConstants.*;

/**
 * Created by NightPlex on 4/22/2019
 * Github: https://github.com/NightPlex
 */
@RestController
@RequestMapping("/authorize")
public class AuthorizationController {

    @Resource
    private UserService userService;

    @Resource
    private ResponseHandlingService responseHandlingService;

    @PostMapping
    public void guestAuthorize(HttpServletRequest req, HttpServletResponse res) {

        String header = req.getHeader(HEADER_STRING);
        if (!StringUtils.isEmpty(header) && header.startsWith(TOKEN_PREFIX)) {
            responseHandlingService.writeErrorMessage(res, "Token already exists, you don't need one", ErrorResponse.INVALID_TOKEN);
        }

        User user = userService.registerGuestUser();
        String token = JWT.create()
                .withSubject(user.getEmail())
                .sign(HMAC512(CLIENT_SECRET.getBytes()));
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
        responseHandlingService.writeSuccessLoginMessage(res, "Successfully signed in and authorized", Collections.emptyList(), TOKEN_PREFIX + token);
    }

}
