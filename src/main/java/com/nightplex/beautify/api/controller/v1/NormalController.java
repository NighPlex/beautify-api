package com.nightplex.beautify.api.controller.v1;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by NightPlex on 4/14/2019
 * Github: https://github.com/NightPlex
 */
@RestController
public class NormalController {

    @GetMapping(value = "/test1")
    public String test1() {
        return "test1";
    }

    @GetMapping(value = "/test2")
    public String test2() {
        return "test2";
    }

}
